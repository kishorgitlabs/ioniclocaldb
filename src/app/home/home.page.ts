import { Component } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

databsobj:SQLiteObject;
readonly databasename :string="fish.db";
readonly tablename:string="details";
mobno:string="";
name:string="";
rowdata:any=[];
editId:boolean=true;
id:number=0;

  constructor(
    private platform:Platform,
    private sqlite:SQLite
  ) {}


async createdb(){
  this.sqlite.create({name:this.databasename,location:'default'}).then((db:SQLiteObject)=>{
    this.databsobj=db;
    this.createtable();
  alert("Databasecreated");
  }).catch((e)=>{
    alert(JSON.stringify(e));
  });
}

async createtable(){
  this.databsobj.executeSql(
    `CREATE TABLE ${this.tablename} (id INTEGER PRIMARY KEY AUTOINCREMENT ,name text,mobno text)`,[]
  ).then(()=>{
    alert("TableCreated");
  }).catch((e)=>{
alert(JSON.stringify(e));
  });

}

async insertrow(){

if(this.editId){
  alert("IF")
  this.databsobj.executeSql(
    `INSERT INTO ${this.tablename} (name,mobno) VALUES ('${this.name}','${this.mobno}')`,[]
  ).then(()=>{
    alert("ROW INSERTED");
  }).catch((e)=>{
alert(JSON.stringify(e));
  });
  this.getrow();
}
else{
alert("ELSE");

this.databsobj.executeSql(
  `UPDATE ${this.tablename} set name = '${this.name}',mobno='${this.mobno}' WHERE id = '${this.id}' `,[]
).then(()=>{
  alert("Updated");
  this.getrow();
}).catch((e)=>{
  alert(JSON.stringify(e));
});

}

}

getrow(){
  this.databsobj.executeSql(
    `SELECT *FROM ${this.tablename}`,[]
  ).then((insert)=>{
alert("Getting");
    this.rowdata=[];

    for(var i=0;i<insert.rows.length;i++){
      this.rowdata.push(insert.rows.item(i));
    }

  }).catch((e)=>{
    alert(JSON.stringify(e));
  });
}

close(a){
  this.databsobj.executeSql(
    `DELETE FROM ${this.tablename} WHERE id = ${a.id}`,[]
  ).then(()=>{
    alert("Deleted");
  }).catch((e)=>{
alert(JSON.stringify(e));
  });
  this.getrow();
}

async edit(a){
  this.name=a.name;
  this.mobno=a.mobno;
  this.id=a.id;
  this.editId=false;
  alert(a.id);
}

}
